<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>身份登入错误 - 我的第一次</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="assets/vendor/bootstrap-v3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/jquery-fullpage/jquery.fullpage.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/errors.css">
</head>
<body style="display:block;">
    <div class="container">
        <div class="errorImage">
            <img src="assets/image/404-18.png" alt="一卡通">
        </div>
        <div class="errorMessage">
            <span class="errorMessage messageChinese">登录错误！</span>
            <span class="errorMessage messageChinese">抱歉，该项服务现只面向集美大学学生用户。</span>
            <a href="/MyFirst/logout"><span class="glyphicon glyphicon-backward"></span> 返回登录页面</a>
            <span class="errorMessage messageChinese">更多“智慧集大”服务</span>
            <a href="http://net.jmu.edu.cn/">去集美大学网络中心看看 <span class="glyphicon glyphicon-forward"></span></a>
        </div>
    </div>
</body>
</html>