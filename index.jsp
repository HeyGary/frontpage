<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>您的第一次</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="assets/vendor/bootstrap-v3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/jquery-fullpage/jquery.fullpage.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <audio id="audio" class="playing" autoplay="autoplay" loop="lopp">
        <source src="assets/bg.mp3" type="audio/mp3" />
        <embed height="100" width="100" src="assets/bg.mp3" />
    </audio>
    <a class="rotation" id="musicControl"><img src="assets/image/music.png" alt="成都" /></a>
    <div id="main">
        <div id="coverPage" class="section">
            <div class="container">
                <div class="coverContent hidden">
                    <span class="coverTitle">集大，我的第一次</span>
                    <span class="coverText">那些年，<br />我在集美大学经历的第一次......</span>
                </div>
                <img src="assets/image/cover.png" alt="集美大学" class="coverImage hidden" />
            </div>
        </div>
        <div id="pg1" class="section">
            <div class="container">
                <div class="pg1Image">
                    <div class="pg1-01 hidden">
                        <img src="assets/image/pg1-02.png" alt="集大尚大楼" />
                    </div>
                </div>
                <div class="pg1Text hidden">
                    <span class="pg1Name">亲爱的<strong class="name"></strong>同学，你好！</span>
                    <span class="pg1Words">
                        <strong class="pg1time"></strong>你加入了集美大学。
                    </span>
                    <span class="pg1Words">白驹过隙，岁月静好，<br />可记得你在集大那些第一次？</span>
                </div>
            </div>
        </div>
        <div id="pg2" class="section">
            <div class="container">
                <div class="pg2Image">
                    <div class="pg2-01 hidden">
                        <img src="assets/image/pg2-01-1.png" alt="消费种类" class="pg2-01-1 hidden" />
                        <img src="assets/image/pg2-01-2.png" alt="一卡通" class="pg2-01-2 hidden" />
                    </div>
                </div>
                <div class="pg2Text hidden">
                    <span class="pg2Intro">食堂、超市、图书馆、校车...校园卡，小小卡片发挥着大大作用。</span>
                    <span class="pg2Words">
                        <strong class="pg2Time"></strong>
                        ，你第一次使用校园卡，在
                        <strong class="location"></strong> 消费了
                        <strong class="cost"></strong> 元。
                    </span>
                </div>
            </div>
        </div>
        <div id="pg3" class="section">
            <div class="container">
                <img src="assets/image/pg3-01.png" alt="集大图书馆" class="pg3Image hidden" />
                <div class="pg3Text hidden">
                    <span class="pg3Intro">翻一页书，粒粒阳光投泻纸上...<br />遇见一本书，邂逅一位哲人。<br /></span>
                    <span class="pg3No hidden">集大图书馆静静等候你的到来。</span>
                    
                    <span class="pg3Library">
                        <strong id="pg3Time1" class="pg3Time"></strong>
                        <br />你第一次来到
                        <strong class="library"></strong>
                    </span>
                    <span class="pg3Book">
                        <strong id="pg3Time2" class="pg3Time"></strong>
                        <br />借走了第一本书：
                        <strong class="book"></strong>
                    </span>
                </div>
            </div>
        </div>
        <div id="pg4" class="section">
            <div class="container">
                <div class="pg4Image">
                    <img src="assets/image/pg4-01.png" class="pg4-01 hidden" alt="五星红旗" />
                    <img src="assets/image/pg4-02.png" class="pg4-02 hidden" alt="握手" />
                </div>
                <div class="pg4Text hidden">
                    <span class="pg4Intro">知悉了你的困难，集大努力帮助。</span>
                    <span class="pg4Help">
                        <strong class="pg4Time"></strong>
                        <br />你第一次获得了<br />
                        <strong class="pg4HelpType"></strong>
                    </span>
                </div>
            </div>
        </div>
        <div id="pg5" class="section">
            <div class="container">
                <div class="pg5Image hidden">
                    <img src="assets/image/pg5-01.png" class="pg5-01" alt="第一次挂科" />
                </div>
                <div class="pg5Text hidden">
                    <span class="pg5Intro">人有失足，马有失蹄。</span>
                    <span class="pg5Down">
                        <strong class="pg5Time"></strong>，你第一次挂科
                        <br />科目是<strong class="pg5Class"></strong>
                    </span>
                </div>
            </div>
        </div>

          <div id="pg6" class="section">
            <div class="container">
                <div class="pg5Image hidden">
                    <img src="assets/image/pg5-01.png" class="pg5-01" alt="第一次挂科" />
                </div>
                <div class="pg5Text hidden">
                    <span class="pg5Intro">人有失足，马有失蹄。</span>
                    <span class="pg5Down">
                        <strong class="pg5Time"></strong>，你第一次挂科
                        <br />科目是<strong class="pg5Class"></strong>
                    </span>
                </div>
            </div>
        </div>



        <div id="endPage" class="section">
            <div class="container">
                <img src="assets/image/endPage-01.png" alt="" class="endPageImage hidden" />
                <div class="endPageText hidden">
                    <span>原来，<br />
                        你在集大已经有了这么多第一次。<br />
                        来日方长，<br />
                        希望你和集美大学一起，<br />
                        创造出更多美好的回忆。
                    </span>
                </div>
                <div class="endPageNetCenter hidden">
                    <span class="netText">“智慧集大”相关服务</span>
                    <a href="http://net.jmu.edu.cn/" id="netCenter">去网络中心首页看看>>></a>
                    <a class="btn tologin" href="/MyFirst/logout">切换账号</a>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/vendor/jquery-v1.11.3/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap-v3.3.1/js/bootstrap.min.js"></script>
    <script src="assets/vendor/raven-v3.14.2/raven.min.js"></script>
    <script>Raven.config('https://607bd962a38842cca6c2697cde5e2e30@sentry.io/160185').install();</script>
    <script src="assets/vendor/jquery-fullpage/jquery.fullpage.min.js"></script>
    <script>
        var first_xh        = "${requestScope.V_XS_DYC.xh}";        /* 学号 */
        var first_xm        = "${requestScope.V_XS_DYC.xm}";        /* 姓名 */
        var first_fv        = "${requestScope.V_XS_DYC.fv}";        /* 图书馆 */
        var first_vjgg      = "${requestScope.V_XS_DYC.vjgg}";      /* 嘉庚图书馆 */
        var first_vygg      = "${requestScope.V_XS_DYC.vygg}";      /* 延奎图书馆 */
        var first_rxxn      = "${requestScope.V_XS_DYC.rxxn}";      /* 入学学年 */
        var first_xfje      = "${requestScope.V_XS_DYC.xfje}";      /* 消费金额 */
        var first_xfsj      = "${requestScope.V_XS_DYC.xfsj}";      /* 消费时间 */
        var first_xffs      = "${requestScope.V_XS_DYC.xffs}";      /* 消费方式 */
        var first_xfdd      = "${requestScope.V_XS_DYC.xfdd}";      /* 消费地点 */
        var first_sjmc      = "${requestScope.V_XS_DYC.sjmc}";      /* 书籍名称 */
        var first_jcrq      = "${requestScope.V_XS_DYC.jcrq}";      /* 借出日期 */
        var first_tsdd      = "${requestScope.V_XS_DYC.tsdd}";      /* 图书地点 */
        var first_gkkm      = "${requestScope.V_XS_DYC.gkkm}";      /* 挂科科目 */
        var first_gkxq      = "${requestScope.V_XS_DYC.gkxq}";      /* 挂科学期 */
        var first_gkfs      = "${requestScope.V_XS_DYC.gkfs}";      /* 挂科分数 */
        var first_zxjmc     = "${requestScope.V_XS_DYC.zxjmc}";     /* 助学金名称 */
        var first_zxjje     = "${requestScope.V_XS_DYC.zxjje}";     /* 助学金金额 */
        var first_zxjpdxn   = "${requestScope.V_XS_DYC.zxjpdxn}";   /* 助学金评定学年 */
        var first_zxjpdxq   = "${requestScope.V_XS_DYC.zxjpdxq}";   /* 助学金评定学期 */
    </script>
    <script src="assets/js/index.js"></script>
</body>
</html>